//
//  ChatViewController.m
//  SkiddooTestProject
//
//  Created by Richard Wu on 27/11/2015.
//  Copyright © 2015 WP. All rights reserved.
//

#import "ChatViewController.h"
#import "ChatDetailsCell.h"
#import "ChatDetails.h"
#import "GlobalFunctions.h"


@interface ChatViewController ()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *msgTXT;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ChatViewController {
    NSMutableArray *chatMsgList;
    BOOL fieldAlreadyUp;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = self.discussionTopic;
    
    chatMsgList = [[NSMutableArray alloc]init];
    [chatMsgList addObjectsFromArray:[[GlobalFunctions shared]retrieveChatDetailsFromFile:self.chatFilePath]];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return chatMsgList.count;
}


 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
 
 // Configure the cell...
     ChatDetailsCell * cell = [tableView dequeueReusableCellWithIdentifier:@"chatCell"];
     
     ChatDetails *details = (ChatDetails *)[chatMsgList objectAtIndex:indexPath.row];
     if ([details.side isEqualToString:@"Me"]) {
         cell.leftMsgLabel.hidden = NO;
         cell.leftMsgLabel.text = details.message;
         cell.rightMsgLabel.hidden = YES;
     } else {
         cell.leftMsgLabel.hidden = YES;
         cell.rightMsgLabel.text = details.message;
         cell.rightMsgLabel.hidden = NO;
     }
 
 return cell;
 }

#pragma UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (!fieldAlreadyUp) {
        [self animateTextField: textField up: YES];
        fieldAlreadyUp = YES;
    }
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSCharacterSet *notAllowedSet = [NSCharacterSet characterSetWithCharactersInString:@"#*:"];
    
    if (range.length > 0 && [string length] == 0) {
        return YES;
    }
    
    
    // set the text field value manually
    NSString *newValue = [[textField text] stringByReplacingCharactersInRange:range withString:string];
    newValue = [[newValue componentsSeparatedByCharactersInSet:notAllowedSet] componentsJoinedByString:@""];
    textField.text = newValue;
    // return NO because we're manually setting the value
    return NO;
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    [self animateTextField: textField up: NO];
    fieldAlreadyUp = NO;

    [[GlobalFunctions shared] appendChatInfo:textField.text fromParty:@"Other" toFile:self.chatFilePath];
    ChatDetails *details = [[ChatDetails alloc] init];
    details.side = @"Other";
    details.message = textField.text;
    [chatMsgList addObject:details];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:chatMsgList.count - 1 inSection:0];
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject: indexPath] withRowAnimation:UITableViewRowAnimationBottom];
    [self.tableView endUpdates];
    
    
    [textField resignFirstResponder];
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    
    //CGRect keyboardBounds;
    
    const int movementDistance = textField.frame.origin.y / 2.5 + 10;
    const float movementDuration = 0.3f;
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

@end
