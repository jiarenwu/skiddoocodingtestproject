//
//  ChatDetailsCell.h
//  SkiddooTestProject
//
//  Created by Richard Wu on 27/11/2015.
//  Copyright © 2015 WP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatDetailsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *leftMsgLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightMsgLabel;

@end
