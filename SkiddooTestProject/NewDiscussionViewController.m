//
//  NewDiscussionViewController.m
//  SkiddooTestProject
//
//  Created by Richard Wu on 27/11/2015.
//  Copyright © 2015 WP. All rights reserved.
//

#import "NewDiscussionViewController.h"
#import "NewDiscussionCell.h"
#import "ContactDetails.h"
#import "GlobalFunctions.h"
#import "ChatViewController.h"
#import "MBProgressHUD.h"

@import AddressBook;
@import Contacts;




@interface NewDiscussionViewController ()<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *discussionTopicTxt;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)cancelTapped:(id)sender;
@end

@implementation NewDiscussionViewController {
    NSMutableArray *contactList;
    BOOL createdTopic;
    NSString *chatFilePath;
    MBProgressHUD *HUD;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.opacity = 0.3;
    [self.view addSubview:HUD];
    HUD.labelText = @"Retrieving contact list ...";
    
    contactList = [[NSMutableArray alloc] init];
    createdTopic = NO;
    [self getContactList];
}
-(void)showHUD {
    [HUD show:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    ChatViewController *chatVc = segue.destinationViewController;
    
    ContactDetails *details = (ContactDetails *)[contactList objectAtIndex:self.tableView.indexPathForSelectedRow.row];
    chatFilePath = [[GlobalFunctions shared] createChatFileWithContactRecordId:details.recordID topic:self.discussionTopicTxt.text];
    [[GlobalFunctions shared] appendChatInfo:@"Hello" fromParty:@"Me" toFile:chatFilePath];
    chatVc.chatFilePath = chatFilePath;
    chatVc.discussionTopic = self.discussionTopicTxt.text;
    createdTopic = YES;
    
}
- (void)listPeopleInAddressBook:(ABAddressBookRef)addressBook
{
    NSArray *allPeople = CFBridgingRelease(ABAddressBookCopyArrayOfAllPeople(addressBook));
    NSInteger numberOfPeople = [allPeople count];
    
    for (NSInteger i = 0; i < numberOfPeople; i++) {
        ABRecordRef person = (__bridge ABRecordRef)allPeople[i];
        
        //only for person
        if (ABRecordGetRecordType(person) == kABPersonType) {
            NSString *firstName = (__bridge NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty)==nil?@"":(__bridge NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
            NSString *lastName = (__bridge NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty)==nil?@"":(__bridge NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
            int recordID = ABRecordGetRecordID(person);
            
            ContactDetails *contactDetails = [[ContactDetails alloc] init];
            contactDetails.recordID = [NSString stringWithFormat:@"%d", recordID];
            contactDetails.userName =[NSString stringWithFormat:@"%@ %@", firstName,lastName];
            
            if (ABPersonHasImageData(person)) {
                if ( &ABPersonCopyImageDataWithFormat != nil ) {
                    contactDetails.avatarImg= [UIImage imageWithData:(__bridge NSData *)ABPersonCopyImageDataWithFormat(person, kABPersonImageFormatThumbnail)];
                }
            }
            //Get main phone number kABPersonPhoneMainLabel
            ABMultiValueRef phones = ABRecordCopyValue(person, kABPersonPhoneProperty);
            for(CFIndex j = 0; j < ABMultiValueGetCount(phones); j++)
            {
                CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(phones, j);
                CFStringRef locLabel = ABMultiValueCopyLabelAtIndex(phones, j);
                NSString *phoneLabel =(__bridge NSString*) ABAddressBookCopyLocalizedLabel(locLabel);
                //CFRelease(phones);
                
                if ([phoneLabel isEqualToString:NSLocalizedString(@"mobile", nil)]) {
                    contactDetails.mobile = (__bridge NSString *)phoneNumberRef;
                    CFRelease(phoneNumberRef);
                    CFRelease(locLabel);
                    break;
                }
            }
            CFRelease(phones);
            
            [contactList addObject:contactDetails];
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView  reloadData];
        [HUD hide:YES];
    });
    
}


- (NSArray *)getContactList {
    //Get current contact list
    if (kiOSVersion > 9.0) {
        CNAuthorizationStatus status = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];
        if (status == CNAuthorizationStatusDenied || status == CNAuthorizationStatusDenied) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"This app previously was refused permissions to contacts; Please go to settings and grant permission to this app so it can use contacts" preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:TRUE completion:nil];
            return nil;
        }
        
        CNContactStore *store = [[CNContactStore alloc] init];
        [store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
            
            // make sure the user granted us access
            
            if (!granted) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    // user didn't grant access;
                    // so, again, tell user here why app needs permissions in order  to do it's job;
                    // this is dispatched to the main queue because this request could be running on background thread
                    [[[UIAlertView alloc] initWithTitle:nil message:@"This app requires access to your contacts to function properly. Please visit to the \"Privacy\" section in the iPhone Settings app." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                });
                return;
            } else {
                [NSThread detachNewThreadSelector:@selector(showHUD) toTarget:self withObject:nil];
                //keys with fetching properties
                NSArray *keys = @[CNContactFamilyNameKey, CNContactGivenNameKey, CNContactPhoneNumbersKey,CNContactTypeKey, CNContactImageDataKey];
                NSString *containerId = store.defaultContainerIdentifier;
                NSPredicate *predicate = [CNContact predicateForContactsInContainerWithIdentifier:containerId];
                NSError *error;
                NSArray *cnContacts = [store unifiedContactsMatchingPredicate:predicate keysToFetch:keys error:&error];
                if (error) {
                    NSLog(@"error fetching contacts %@", error);
                } else {
                    for (CNContact *contact in cnContacts) {
                        if (contact.contactType == CNContactTypePerson) {
                            ContactDetails *newContact = [[ContactDetails alloc] init];
                            newContact.userName = [NSString stringWithFormat:@"%@ %@",contact.givenName, contact.familyName];
                            newContact.recordID = contact.identifier;
                            UIImage *image = [UIImage imageWithData:contact.imageData];
                            newContact.avatarImg = image;
                            for (CNLabeledValue *label in contact.phoneNumbers) {
                                NSString *phone = [label.value stringValue];
                                if ([phone length] > 0) {
                                    newContact.mobile = phone;
                                    break;
                                }
                            }
                            [contactList addObject:newContact];
                        }
                        
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.tableView reloadData];
                        [HUD hide:YES];
                    });
                }
                
            }
            
        }];
    } else {
        //Under ios 9.0
        ABAuthorizationStatus status = ABAddressBookGetAuthorizationStatus();
        
        if (status == kABAuthorizationStatusDenied || status == kABAuthorizationStatusRestricted) {
            // if you got here, user had previously denied/revoked permission for your
            // app to access the contacts, and all you can do is handle this gracefully,
            // perhaps telling the user that they have to go to settings to grant access
            // to contacts
            
            [[[UIAlertView alloc] initWithTitle:nil message:@"This app requires access to your contacts to function properly. Please visit to the \"Privacy\" section in the iPhone Settings app." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            return nil;
        }
        
        CFErrorRef error = NULL;
        ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, &error);
        
        if (!addressBook) {
            NSLog(@"ABAddressBookCreateWithOptions error: %@", CFBridgingRelease(error));
            return nil;
        }
        
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            if (error) {
                NSLog(@"ABAddressBookRequestAccessWithCompletion error: %@", CFBridgingRelease(error));
            }
            
            if (granted) {
                [NSThread detachNewThreadSelector:@selector(showHUD) toTarget:self withObject:nil];
                [self listPeopleInAddressBook:addressBook];
            } else {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    // BTW, this is not on the main thread, so dispatch UI updates back to the main queue
                    
                    [[[UIAlertView alloc] initWithTitle:nil message:@"This app requires access to your contacts to function properly. Please visit to the \"Privacy\" section in the iPhone Settings app." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                });
            }
            
            CFRelease(addressBook);
        });
    }
    return nil;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return contactList.count;
}


 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
 
 // Configure the cell...
     NewDiscussionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"contactDetailsCell"];
     ContactDetails *contactDetails = (ContactDetails *)[contactList objectAtIndex:indexPath.row];
     cell.avatarImgView.image = contactDetails.avatarImg;
     cell.userName.text = contactDetails.userName;
     cell.userMobile.text = contactDetails.mobile;
      
 
 return cell;
}


#pragma mark - UitableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.discussionTopicTxt.text.length < 1) {
        //empty
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Stop" message:@"Please enter topic to continue ..." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    [self performSegueWithIdentifier:@"bringupNewChat" sender:self];
}


#pragma UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSCharacterSet *notAllowedSet = [NSCharacterSet characterSetWithCharactersInString:@"#*:"];
    
    if (range.length > 0 && [string length] == 0) {
        return YES;
    }
    
    
    // set the text field value manually
    NSString *newValue = [[textField text] stringByReplacingCharactersInRange:range withString:string];
    newValue = [[newValue componentsSeparatedByCharactersInSet:notAllowedSet] componentsJoinedByString:@""];
    textField.text = newValue;
    // return NO because we're manually setting the value
    return NO;
    
}

- (IBAction)cancelTapped:(id)sender {
    if (createdTopic) {
        if (self.delegate) {
            ContactDetails *details = (ContactDetails *)[contactList objectAtIndex:self.tableView.indexPathForSelectedRow.row];
            
            [self.delegate didAddTopic:self.discussionTopicTxt.text recordId:details.recordID fullName:details.userName image:details.avatarImg date:[NSDate date] chatFilePath:chatFilePath];
        }
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
