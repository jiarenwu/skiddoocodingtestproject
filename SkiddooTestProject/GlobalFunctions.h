//
//  GlobalFunctions.h
//  SkiddooTestProject
//
//  Created by Richard Wu on 26/11/2015.
//  Copyright © 2015 WP. All rights reserved.
//

#import <Foundation/Foundation.h>
#define kAvatarImagesFolder @"avatarImages"
#define kUserListFolder @"userList"
#define kDiscussionTopicsFolder @"discussion Topics"
#define kiOSVersion    [[[UIDevice currentDevice] systemVersion] floatValue]

@interface GlobalFunctions : NSObject
+ (GlobalFunctions *)shared;

-(NSMutableArray *)retrieveAllChatsTopicDetails;
-(void)deleteFromDocumentdirectoryFile:(NSString *)fileName;
- (NSString *)documentsPathForFileName:(NSString *)name;
- (NSString *)createChatFileWithContactRecordId:(NSString *)recordId topic:(NSString *)topic;
- (void)appendChatInfo:(NSString *)msg fromParty:(NSString *)side toFile:(NSString *)file;
- (NSArray *)retrieveChatDetailsFromFile:(NSString *)file;
@end
