//
//  MainDiscussionCell.h
//  SkiddooTestProject
//
//  Created by Richard Wu on 26/11/2015.
//  Copyright © 2015 WP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainDiscussionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *avatarImgView;
@property (weak, nonatomic) IBOutlet UILabel *PersonName;
@property (weak, nonatomic) IBOutlet UILabel *topic;
@property (weak, nonatomic) IBOutlet UILabel *startTime;

@end
