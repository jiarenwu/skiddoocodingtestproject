//
//  ContactDeails.h
//  SkiddooTestProject
//
//  Created by Richard Wu on 26/11/2015.
//  Copyright © 2015 WP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ContactDetails : NSObject
@property (nonatomic,strong) NSString *recordID;
@property (nonatomic,strong)UIImage *avatarImg;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *mobile;
@end
