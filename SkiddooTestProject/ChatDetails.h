//
//  ChatDetails.h
//  SkiddooTestProject
//
//  Created by Richard Wu on 27/11/2015.
//  Copyright © 2015 WP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface ChatDetails : NSObject

@property (nonatomic, strong) NSString *side;
@property (nonatomic, strong) NSString *message;
@end
