//
//  DiscussionViewController.h
//  SkiddooTestProject
//
//  Created by Richard Wu on 27/11/2015.
//  Copyright © 2015 WP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewDiscussionViewController.h"

@interface DiscussionViewController : UITableViewController <NewDiscussionViewControllerDelegate>

@end
