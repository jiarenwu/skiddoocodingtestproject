//
//  NewDiscussionViewController.h
//  SkiddooTestProject
//
//  Created by Richard Wu on 27/11/2015.
//  Copyright © 2015 WP. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NewDiscussionViewControllerDelegate <NSObject>
@optional
-(void)didAddTopic:(NSString *)topic recordId: (NSString *)recordID fullName:(NSString *)fullName image:(UIImage *)img date:(NSDate *)startDate chatFilePath:(NSString *)filePath;
@end
@interface NewDiscussionViewController : UIViewController

@property (weak,nonatomic) id <NewDiscussionViewControllerDelegate> delegate;
@end
