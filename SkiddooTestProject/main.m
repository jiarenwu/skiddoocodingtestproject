//
//  main.m
//  SkiddooTestProject
//
//  Created by Richard Wu on 26/11/2015.
//  Copyright © 2015 WP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
