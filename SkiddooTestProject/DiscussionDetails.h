//
//  DiscussionDetails.h
//  SkiddooTestProject
//
//  Created by Richard Wu on 26/11/2015.
//  Copyright © 2015 WP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface DiscussionDetails : NSObject
@property (nonatomic, strong)NSString *recordID;
@property (nonatomic, strong)NSString *topic;
@property (nonatomic, strong)NSDate *startTime;

@property (nonatomic,strong)NSString *chatFileName;

@end
