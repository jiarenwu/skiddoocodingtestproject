//
//  DiscussionViewController.m
//  SkiddooTestProject
//
//  Created by Richard Wu on 27/11/2015.
//  Copyright © 2015 WP. All rights reserved.
//

#import "DiscussionViewController.h"
#import "MainDiscussionCell.h"
#import "DiscussionDetails.h"
#import "GlobalFunctions.h"
#import "ChatViewController.h"
#import "UIAlertView+NSCookbook.h"
#import "MBProgressHUD.h"

@import AddressBook;
@import Contacts;


@interface ProfileDetails : NSObject
@property (nonatomic, strong)UIImage *avatarImg;
@property (nonatomic, strong)NSString *fullName;
@end

@implementation ProfileDetails

@end

@interface DiscussionViewController ()

@end

@implementation DiscussionViewController {
    NSMutableArray *discussionList;
    NSMutableArray *profileList;
    NSDateFormatter *format;
    MBProgressHUD *HUD;
}

- (void)getProfileDetails {
    //Get current contact list
    if (kiOSVersion > 9.0) {
        CNAuthorizationStatus status = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];
        if (status == CNAuthorizationStatusDenied || status == CNAuthorizationStatusDenied) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"This app previously was refused permissions to contacts; Please go to settings and grant permission to this app so it can use contacts" preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:TRUE completion:nil];
            return;
        }
        
        CNContactStore *store = [[CNContactStore alloc] init];
        NSArray *keys = @[CNContactFamilyNameKey, CNContactGivenNameKey, CNContactPhoneNumbersKey,CNContactTypeKey, CNContactImageDataKey];
        NSError *error = nil;
        
        [NSThread detachNewThreadSelector:@selector(showHUD) toTarget:self withObject:nil];
        for (DiscussionDetails *details in discussionList) {
            CNContact *contact = [store  unifiedContactWithIdentifier:details.recordID keysToFetch:keys error:&error];
            if (! error && contact) {
                ProfileDetails *profile = [[ProfileDetails alloc] init];
                
                profile.fullName =[NSString stringWithFormat:@"%@ %@", contact.givenName,contact.familyName];
                UIImage *image = [UIImage imageWithData:contact.imageData];
                profile.avatarImg = image;
                [profileList addObject:profile];
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
            [HUD hide:YES];
        });
        
    } else {
        //Under ios 9.0
        ABAuthorizationStatus status = ABAddressBookGetAuthorizationStatus();
        
        if (status == kABAuthorizationStatusDenied || status == kABAuthorizationStatusRestricted) {
            // if you got here, user had previously denied/revoked permission for your
            // app to access the contacts, and all you can do is handle this gracefully,
            // perhaps telling the user that they have to go to settings to grant access
            // to contacts
            
            [[[UIAlertView alloc] initWithTitle:nil message:@"This app requires access to your contacts to function properly. Please visit to the \"Privacy\" section in the iPhone Settings app." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            return;
        }
        
        CFErrorRef error = NULL;
        ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, &error);
        
        if (!addressBook) {
            NSLog(@"ABAddressBookCreateWithOptions error: %@", CFBridgingRelease(error));
            return;
        }
        
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            if (error) {
                NSLog(@"ABAddressBookRequestAccessWithCompletion error: %@", CFBridgingRelease(error));
            }
            
            if (granted) {
                
                NSString *firstName;
                NSString *lastName;
                [NSThread detachNewThreadSelector:@selector(showHUD) toTarget:self withObject:nil];
                for (DiscussionDetails *details in discussionList) {
                    ABRecordRef person = ABAddressBookGetPersonWithRecordID(addressBook, [details.recordID intValue]);
                    
                    firstName = (__bridge NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty)==nil?@"":(__bridge NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
                    lastName = (__bridge NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty)==nil?@"":(__bridge NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
                    
                    ProfileDetails *profile = [[ProfileDetails alloc] init];
                    
                    profile.fullName =[NSString stringWithFormat:@"%@ %@", firstName,lastName];
                    
                    if (ABPersonHasImageData(person)) {
                        if ( &ABPersonCopyImageDataWithFormat != nil ) {
                            profile.avatarImg= [UIImage imageWithData:(__bridge NSData *)ABPersonCopyImageDataWithFormat(person, kABPersonImageFormatThumbnail)];
                        }
                    }
                    [profileList addObject:profile];
                    
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.tableView reloadData];
                    [HUD hide:YES];
                });
                
            }
            
            CFRelease(addressBook);
        });
    }
    
}
- (void)getAllDiscustionDetails {
    discussionList = [[GlobalFunctions shared] retrieveAllChatsTopicDetails];
    [self getProfileDetails];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.opacity = 0.3;
    [self.view addSubview:HUD];
    HUD.labelText = @"Retrieving discussion list ...";

    discussionList = [[NSMutableArray alloc] init];
    profileList = [[NSMutableArray alloc] init];
    format = [[NSDateFormatter alloc] init];
    format.dateFormat = @"dd/MM/yyyy";
    
    [self getAllDiscustionDetails];
    
}
-(void)showHUD {
    [HUD show:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return discussionList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    MainDiscussionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mainDicussionCell"];
    DiscussionDetails *details = (DiscussionDetails *)[discussionList objectAtIndex:indexPath.row];
    ProfileDetails *profile = (ProfileDetails *) [profileList objectAtIndex:indexPath.row];
    
    
    
    cell.avatarImgView.image = profile.avatarImg;
    cell.PersonName.text = profile.fullName;
    cell.topic.text = details.topic;
    cell.startTime.text = [format stringFromDate:details.startTime];
    
    
    return cell;
}


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"CONFIRMATION",nil) message:NSLocalizedString(@"Do you want to delete this topic with all messages?",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"NO",nil)
                                                  otherButtonTitles:NSLocalizedString(@"Yes",nil), nil];
        
        [alertView showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
            switch (buttonIndex) {
                case 0:
                    break;
                case 1:{
                    int curIndex = indexPath.row;
                    
                    DiscussionDetails *details = (DiscussionDetails *)[discussionList objectAtIndex:curIndex];
                    NSString *filePath = details.chatFileName;
                    [[GlobalFunctions shared] deleteFromDocumentdirectoryFile:filePath];
                    [discussionList removeObjectAtIndex:curIndex];
                    [profileList removeObjectAtIndex:curIndex];
                    
                    [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]   withRowAnimation:UITableViewRowAnimationFade];
                  }
                    
                    break;
                default:
                    break;
            }
        }];
        
        //[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"bringupChatDetailsscreen"]) {
        ChatViewController *chatVc = (ChatViewController *)segue.destinationViewController;
        DiscussionDetails *details = (DiscussionDetails *)[discussionList objectAtIndex:self.tableView.indexPathForSelectedRow.row];
        
        chatVc.chatFilePath = details.chatFileName;
        chatVc.discussionTopic = details.topic;
    } else {
        UINavigationController *nav = (UINavigationController *)segue.destinationViewController;
        NewDiscussionViewController *discussionVc = (NewDiscussionViewController *) [nav topViewController];
        discussionVc.delegate = self;
    }
}

#pragma mark - NewDiscussionViewControllerDelegate
- (void)didAddTopic:(NSString *)topic recordId:(NSString *)recordID fullName:(NSString *)fullName image:(UIImage *)img date:(NSDate *)startDate chatFilePath:(NSString *)filePath {
    DiscussionDetails *discussionDetails = [[DiscussionDetails alloc] init];
    discussionDetails.recordID = recordID;
    discussionDetails.topic = topic;
    discussionDetails.startTime = startDate;
    discussionDetails.chatFileName = filePath;
    
    ProfileDetails *profile = [[ProfileDetails alloc] init];
    profile.fullName = fullName;
    profile.avatarImg = img;
    [discussionList addObject:discussionDetails];
    [profileList addObject:profile];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:discussionList.count - 1 inSection:0];
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject: indexPath] withRowAnimation:UITableViewRowAnimationBottom];
    
    [self.tableView endUpdates];
}

@end
