//
//  GlobalFunctions.m
//  SkiddooTestProject
//
//  Created by Richard Wu on 26/11/2015.
//  Copyright © 2015 WP. All rights reserved.
//

#import "GlobalFunctions.h"
#import "ChatDetails.h"
#import "DiscussionDetails.h"

@implementation GlobalFunctions


static GlobalFunctions *_shared = nil;

+ (GlobalFunctions *)shared {
    static dispatch_once_t onceToken;
    
    if (!_shared) {
        dispatch_once(&onceToken, ^{
            _shared = [[GlobalFunctions alloc] init];
        });
        
    }
    return _shared;
}


-(void)deleteFromDocumentdirectoryFile:(NSString *)fileName {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    [fileManager removeItemAtPath:fileName error:&error];
    if (error) {
        NSLog(@"When deleting file %@, error happen %@",fileName, error.localizedDescription);
    }
}

- (NSString *)documentsPathForFileName:(NSString *)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    
    return [documentsPath stringByAppendingPathComponent:name];
}
- (BOOL)isFileNameExist:(NSString *)fileName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSString *file = [documentsDir stringByAppendingPathComponent:fileName];;

    return [fileManager fileExistsAtPath:file];
}

- (NSString *)getFileSufixFromDailyCount:(NSString *)countStr {
    NSMutableString *fileSufix = [NSMutableString stringWithCapacity:4];
    [fileSufix appendString:countStr];
    for (int length = countStr.length; length < 3; length ++) {
        [fileSufix insertString:@"0" atIndex:0];
    }
    return fileSufix;
}
- (NSString *)getFileName {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"ddMMyyyy";
    
    NSString *baseFileName = [dateFormatter stringFromDate:[NSDate date]];
    
    NSString *fileSuffix = @"";
    int dailyCount = 0;
    do {
        dailyCount ++;
        fileSuffix = [self getFileSufixFromDailyCount:[NSString stringWithFormat:@"%d", dailyCount]];
        
    } while ([self isFileNameExist:[NSString stringWithFormat:@"%@%@",baseFileName, fileSuffix]]);
        
    return [NSString stringWithFormat:@"%@%@",baseFileName, fileSuffix];
}
- (NSString *)createFileForName:(NSString *)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSString *file = [documentsPath stringByAppendingPathComponent:name];
    [fileManager createFileAtPath:file contents:nil attributes:nil];
    return file;
}

- (NSString *)createChatFileWithContactRecordId:(NSString *)recordId topic:(NSString *)topic {
    //data layout for chat would be: Date,Other person contact Record ID, topic#Me:message*Other:message*Me:message*Other:message
    NSString *fileName = [self getFileName];
    NSString *chatFile = [self createFileForName:fileName];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd/MM/yyyy";
    
    NSString *stringToWrite = [NSString stringWithFormat:@"%@,%@,%@#", [dateFormatter stringFromDate:[NSDate date]],recordId, topic];
    [stringToWrite writeToFile:chatFile atomically:YES encoding:NSUTF8StringEncoding error:nil];
    
    return chatFile;
}

- (void)appendChatInfo:(NSString *)msg fromParty:(NSString *)side toFile:(NSString *)file {
    NSFileHandle *fileHandler = [NSFileHandle fileHandleForUpdatingAtPath:file];
    
    NSString *stringToWrite = [NSString stringWithFormat:@"%@:%@*", side, msg];
    
    [fileHandler seekToEndOfFile];
    [fileHandler writeData:[NSData dataWithData:[stringToWrite dataUsingEncoding:NSUTF8StringEncoding]]];
}
- (NSArray *)retrieveChatDetailsFromFile:(NSString *)file {
    NSMutableArray *retArray = [[NSMutableArray alloc] init];
    
    NSError *error = nil;
    NSString *chatStrings = [NSString stringWithContentsOfFile:file encoding:NSUTF8StringEncoding error:&error];
    
    if (! error) {
         NSArray *originalChatsArray = [chatStrings componentsSeparatedByString:@"#"];
        if (originalChatsArray.count > 1) {
            NSString *chatDetailsString = [originalChatsArray objectAtIndex:1];
            NSArray *chatsDetailsArray = [chatDetailsString componentsSeparatedByString:@"*"];
            for (NSString *chat in chatsDetailsArray) {
                if (chat.length > 1) {
                    NSArray *individualChatArray = [chat componentsSeparatedByString:@":"];
                    ChatDetails *details = [[ChatDetails alloc] init];
                    details.side = [individualChatArray objectAtIndex:0];
                    details.message = [individualChatArray objectAtIndex:1];
                    [retArray addObject:details];
                }
            }
        }
    }
    
    return retArray;
}


-(NSMutableArray *)retrieveAllChatsTopicDetails {
    NSMutableArray *retval = [[NSMutableArray alloc] init];
    NSError *error;
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    
    
    NSFileManager *manager = [NSFileManager defaultManager];
    NSArray *fileList = [manager contentsOfDirectoryAtPath:documentsDir error:&error];
    
    if (error) {
        return nil;
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd/MM/yyyy";
    for (NSString *file in fileList) {
        NSString *chatStrings = [NSString stringWithContentsOfFile:[documentsDir stringByAppendingPathComponent:file] encoding:NSUTF8StringEncoding error:&error];
        
        if (! error) {
            NSArray *originalChatsArray = [chatStrings componentsSeparatedByString:@"#"];
            if (originalChatsArray.count > 0) {
                NSString *chatHeadString = [originalChatsArray objectAtIndex:0];
                NSArray *chatHeadsArray = [chatHeadString componentsSeparatedByString:@","];
                
                if (chatHeadsArray.count < 3) {
                    continue;
                }
                DiscussionDetails *details = [[DiscussionDetails alloc] init];
                details.startTime = [dateFormatter dateFromString:[chatHeadsArray objectAtIndex:0]];
                details.recordID = [chatHeadsArray objectAtIndex:1];
                details.topic  = [chatHeadsArray objectAtIndex:2];
                details.chatFileName = [documentsDir stringByAppendingPathComponent:file];
                
                [retval addObject:details];
            }
        }
    }
    return retval;
}

@end
