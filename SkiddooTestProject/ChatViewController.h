//
//  ChatViewController.h
//  SkiddooTestProject
//
//  Created by Richard Wu on 27/11/2015.
//  Copyright © 2015 WP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatViewController : UIViewController

@property (nonatomic, strong) NSString *chatFilePath;
@property (nonatomic,strong) NSString *discussionTopic;
@end
